﻿namespace IntermediateDotNet.ColFx
{
    public abstract class BiSeqBase<T> : IBiSeq<T>
    {
        ISeq<T> ISeq<T>.Next
        {
            get { return Next; }
        }

        public abstract IBiSeq<T> Previous { get; }

        public abstract IBiSeq<T> Next { get; }
        public abstract T Current { get; }
    }
}
