﻿using System.Collections.Generic;

namespace IntermediateDotNet.ColFx
{
    public static class Collections
    {
        public static void AddAll<T>(this ICollection<T> target, IRange<ISeq<T>> source)
        {
            for (var pos = source.First; !pos.Equals(source.Last); pos = pos.Next)
                target.Add(pos.Current);
        }
    }
}
