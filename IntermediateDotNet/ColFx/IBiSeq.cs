namespace IntermediateDotNet.ColFx
{
    public interface IBiSeq<out T> : ISeq<T>
    {
        new IBiSeq<T> Next { get; }
        IBiSeq<T> Previous { get; }
    }
}