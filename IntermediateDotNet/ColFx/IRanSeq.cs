using System;

namespace IntermediateDotNet.ColFx
{
    public interface IRanSeq<T> : IBiSeq<T>, IComparable<IRanSeq<T>>
    {
        new IRanSeq<T> Next { get; }
        new IRanSeq<T> Previous { get; }
        IRanSeq<T> By(int x);
        int Distance(IRanSeq<T> that);
    }
}