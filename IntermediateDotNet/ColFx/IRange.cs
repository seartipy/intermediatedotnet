﻿
namespace IntermediateDotNet.ColFx
{
    public interface IRange<out TIter>
    {
        TIter First { get; }
        TIter Last { get; }
    }
}
