namespace IntermediateDotNet.ColFx
{
    public interface ISeq<out T>
    {
        ISeq<T> Next { get; }
        T Current { get; }
    }
}