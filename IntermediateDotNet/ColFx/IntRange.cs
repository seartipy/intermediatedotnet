﻿namespace IntermediateDotNet.ColFx
{
    public class IntRange : IRange<IRanSeq<int>>
    {
        private readonly IntSeq first;
        private readonly IntSeq last;

        public IntRange(int first, int last)
        {
            this.first = new IntSeq(first);
            this.last = new IntSeq(last);
        }
        IRanSeq<int> IRange<IRanSeq<int>>.First
        {
            get { return first; }
        }

        IRanSeq<int> IRange<IRanSeq<int>>.Last
        {
            get { return last; }
        }
    }
}
