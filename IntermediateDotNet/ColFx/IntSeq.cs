using System;

namespace IntermediateDotNet.ColFx
{
    public class IntSeq : RanSeqBase<int>
    {
        private readonly int current;

        protected bool Equals(IntSeq other)
        {
            return Current == other.Current;
        }

        public override int GetHashCode()
        {
            return Current.GetHashCode();
        }

        public IntSeq(int current)
        {
            this.current = current;
        }

        public  override int Current { get { return current; } }

        public override IRanSeq<int> Previous
        {
            get { return new IntSeq(Current - 1); }
        }

        public override IRanSeq<int> By(int x)
        {
            return new IntSeq(Current + x);
        }

        public override int Distance(IRanSeq<int> that)
        {
            return that.Current - Current;
        }

        public override IRanSeq<int> Next
        {
            get { return new IntSeq(Current + 1); }
        }

        public override int CompareTo(IRanSeq<int> other)
        {
            return other.Distance(this);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != this.GetType())
                return false;
            return Current == ((IntSeq) obj).Current;
        }
    }
}