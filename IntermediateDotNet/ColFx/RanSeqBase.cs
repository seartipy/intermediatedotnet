﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntermediateDotNet.ColFx
{
    public abstract class RanSeqBase<T> : IRanSeq<T>
    {
        public abstract IRanSeq<T> Next { get; }
        IBiSeq<T> IBiSeq<T>.Previous
        {
            get { return Previous; }
        }

        IBiSeq<T> IBiSeq<T>.Next
        {
            get { return Next; }
        }

        public abstract T Current { get; }

        public abstract IRanSeq<T> Previous { get; }
        public abstract IRanSeq<T> By(int x);
        public abstract int Distance(IRanSeq<T> that);
        ISeq<T> ISeq<T>.Next
        {
            get { return Next; }
        }

        public abstract int CompareTo(IRanSeq<T> other);
    }
}
