namespace IntermediateDotNet.ColFx
{
    public class Range<TIter> : IRange<TIter>
    {
        private readonly TIter first;
        private readonly TIter last;

        public Range(TIter first, TIter last)
        {
            this.first = first;
            this.last = last;
        }

        TIter IRange<TIter>.First
        {
            get { return first; }
        }

        TIter IRange<TIter>.Last
        {
            get { return last; }
        }
    }
}