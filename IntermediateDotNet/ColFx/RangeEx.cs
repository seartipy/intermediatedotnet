﻿using System;
using System.Collections.Generic;

namespace IntermediateDotNet.ColFx
{
    public static class RangeEx
    {
        public static IntRange To(this int from, int to)
        {
            return new IntRange(from, to);
        }

        public static IRange<TIter> Range<TIter>(this TIter first, TIter last)
        {
            return new Range<TIter>(first, last);
        }
        
        public static int Length<T>(this IRange<IRanSeq<T>> range)
        {
            return range.First.Distance(range.Last);
        }

        public static bool Contains<T>(this IRange<ISeq<T>> list, T value)
        {
            for(var pos = list.First; pos.NotEqual(list.Last); pos = pos.Next)
                if (pos.Current.Equals(value))
                    return true;
            return false;
        }

        public static IRanSeq<T> BinarySearch<T>(this IRange<IRanSeq<T>> list, T value)
            where  T : IComparable<T>
        {
            if (list.Length() == 0)
                return list.Last;
            var mid = list.First.By(list.Length() / 2);
            if (value.CompareTo(mid.Current) < 0)
                return mid.Range(mid.Previous).BinarySearch(value);
            if (mid.Current.CompareTo(value) < 0)
                return mid.Next.Range(list.Last).BinarySearch(value);
            return mid;
        }
        public static List<T> Reversed<T>(this IRange<IBiSeq<T>> list)
        {
            var reversed = new List<T>();
            for (var pos = list.Last; pos.NotEqual(list.First); pos = pos.Previous)
                reversed.Add(pos.Previous.Current);
            return reversed;
        }
    }
}
