namespace IntermediateDotNet.ColFx
{
    // Range type tests cannot be done in F#, as it doesn't support covariance/contravariance yet
    static internal class RangeTypeTests
    {
        static void Main()
        {
            new Range<IRanSeq<int>>(new IntSeq(10), new IntSeq(20)).Contains(5);
            new Range<IBiSeq<int>>(new IntSeq(10), new IntSeq(20)).Contains(5);
            new Range<ISeq<int>>(new IntSeq(10), new IntSeq(20)).Contains(5);

            new Range<IRanSeq<int>>(new IntSeq(10), new IntSeq(20)).Reversed();
            new Range<IBiSeq<int>>(new IntSeq(10), new IntSeq(20)).Reversed();

            new Range<IRanSeq<int>>(new IntSeq(10), new IntSeq(20)).BinarySearch(5);
        }
    }
}