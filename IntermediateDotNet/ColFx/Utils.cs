﻿namespace IntermediateDotNet.ColFx
{
    public static class Utils
    {
        public static bool NotEqual<T>(this T left, T right)
        {
            return !left.Equals(right);
        }

        public static IntRange To(this int first, int last)
        {
            return new IntRange(first, last);
        }
    }
}
