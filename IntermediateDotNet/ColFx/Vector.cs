﻿using System;
using System.Collections.Generic;

namespace IntermediateDotNet.ColFx
{
    // Abstract data types encapsulate implementation, almost always implemented as sealed classes.
    public sealed class Vector<T> : IRange<IRanSeq<T>>
    {
        private T[] arr;

        public Vector(params T[] args)
        {
            arr = new T[args.Length];
            Array.Copy(args, arr, args.Length);
            Count = arr.Length;
        }

        public void Push(T x)
        {
            EnsureCapacity(Count + 1);
            arr[Count++] = x;
        }

        public T Pop()
        {
            return arr[--Count];
        }

        public int Count { get; private set; }

        public void Insert(int index, T value)
        {
            EnsureCapacity(Count + 1);
            ShiftRight(index);
            arr[index] = value;
            ++Count;
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index >= Count)
                throw new IndexOutOfRangeException();
            ShiftLeft(index);
            --Count;
        }

        public int IndexOf(T value)
        {
            for(var i = 0; i < arr.Length; ++i)
                if (arr[i].Equals(value))
                    return i;
            return -1;
        }

        public T this[int index]
        {
            get { return arr[index]; }
            set { arr[index] = value; }
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (var i = 0; i < Count; ++i)
                yield return arr[i];
        }

        private void EnsureCapacity(int newCapacity)
        {
            var capacity = Math.Max(2 * arr.Length + 1, newCapacity);
            var t = new T[capacity];
            Array.Copy(arr, t, Count);
            arr = t;
        }

        private void ShiftLeft(int pos)
        {
            for (var i = pos; i < Count - 1; ++i)
                arr[i] = arr[i + 1];
        }

        private void ShiftRight(int pos)
        {
            for (var i = Count; i > pos; --i)
                arr[i] = arr[i - 1];
        }

        IRanSeq<T> IRange<IRanSeq<T>>.First
        {
            get { return new VecSeq<T>(this); }
        }

        IRanSeq<T> IRange<IRanSeq<T>>.Last
        {
            get { return new VecSeq<T>(this, Count); }
        }
    }

    class VecSeq<T> : RanSeqBase<T>
    {
        protected bool Equals(VecSeq<T> other)
        {
            return pos == other.pos;
        }

        public override int GetHashCode()
        {
            return pos.GetHashCode();
        }

        private readonly Vector<T> vector;
        private readonly int pos;

        public VecSeq(Vector<T> vector, int pos)
        {
            this.vector = vector;
            this.pos = pos;
        }

        public VecSeq(Vector<T> vector)
        {
            this.vector = vector;
        }

        public override IRanSeq<T> By(int x)
        {
            return new VecSeq<T>(vector, pos + x);
        }

        public override int Distance(IRanSeq<T> that)
        {
            return ((VecSeq<T>) that).pos - pos;
        }

        public override IRanSeq<T> Next
        {
            get { return new VecSeq<T>(vector, pos + 1); }
        }

        public override int CompareTo(IRanSeq<T> other)
        {
            return -Distance(other);
        }

        public override IRanSeq<T> Previous
        {
            get { return new VecSeq<T>(vector, pos - 1); }
        }

        public override T Current
        {
            get { return vector[pos]; }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;
            return Equals((VecSeq<T>)obj);
        }
    }

}
