﻿namespace IntermediateDotNet.Query
{
    sealed class AndQuery : IQuery
    {
        private readonly IQuery left ;
        private readonly IQuery right;

        public AndQuery(IQuery left, IQuery right)
        {
            this.left = left;
            this.right = right;
        }

        bool IQuery.Apply(string input)
        {
            return left.Apply(input) && right.Apply(input);
        }

        public override string ToString()
        {
            return "( " + left + " && " + right + " )";
        }
    }
}
