﻿using System;
using System.IO;

namespace IntermediateDotNet.Query
{
    class Expr
    {
        static void Main()
        {
            var hello = new WordQuery("hello");
            var world = new WordQuery("world");

            var helloAndWorld = new AndQuery(hello, world);
            var helloOrWorld = new OrQuery(hello, world);
            var notUniverse = new NotQuery(new WordQuery("universe"));


            var helloAndWorldAndNotUniverse = new AndQuery(helloAndWorld, notUniverse);


            var q = new Query("bool") & new Query("IQuery") | new Query("ToStr");
            foreach(var line in q.Eval(File.ReadAllLines(@"..\..\Expr.cs")))
                Console.WriteLine(line);
            Console.ReadKey();
        }
    }
}
