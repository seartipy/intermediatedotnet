﻿namespace IntermediateDotNet.Query
{
    interface IQuery
    {
        bool Apply(string line);
    }
}