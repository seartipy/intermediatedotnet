﻿namespace IntermediateDotNet.Query
{
    sealed class NotQuery : IQuery
    {
        private readonly IQuery query ;

        public NotQuery(IQuery query)
        {
            this.query = query;
        }
        bool IQuery.Apply(string input)
        {
            return !query.Apply(input);
        }

        public override string ToString()
        {
            return "!(" + query + ")";
        }
    }
}