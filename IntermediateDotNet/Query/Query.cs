﻿using System.Collections.Generic;
using System.Linq;

namespace IntermediateDotNet.Query
{
    // Wrap inheritance heirarchy using behind a  whenever possible.
    public sealed class Query
    {
        private readonly IQuery query;

        private Query(IQuery query)
        {
            this.query = query;
        }

        public Query(string word)
        {
            query = new WordQuery(word);
        }

        public IEnumerable<string> Eval(IEnumerable<string> input)
        {
            return input.Where(query.Apply);
        }

        public override string ToString()
        {
            return query.ToString();
        }

        public static Query operator&(Query first, Query second)
        {
            return new Query(new AndQuery(first.query, second.query));
        }

        public static Query operator|(Query first, Query second)
        {
            return new Query(new OrQuery(first.query, second.query));
        }

        public static Query operator!(Query query)
        {
            return new Query(new NotQuery(query.query));
        }
    }
}