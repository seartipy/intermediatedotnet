﻿namespace IntermediateDotNet.Query
{
    // Polymorphism is best implemented using abstractions with crisp boundaries using interfaces. 
    // In this cases, classes must only provide implementation. Prefer explicit interface implementation.
    sealed class WordQuery : IQuery
    {
        private readonly string word;

        public WordQuery(string word)
        {
            this.word = word;
        }

        bool IQuery.Apply(string input)
        {
            return input.Contains(word);
        }

        public override string ToString()
        {
            return word;
        }
    }
}