﻿namespace IntermediateDotNet.Simple
{
    // Represent data using a class with dumb read write properties
    public class Address
    {
        public string HouseNo { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }
}
