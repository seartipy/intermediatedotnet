﻿namespace IntermediateDotNet.Simple
{
    public class Arith
    {
        public static int Factorial(int n)
        {
            var fact = 1;
            for (var i = 2; i <= n; ++i)
                fact *= i;
            return fact;
        }

        public static bool IsPrime(int n)
        {
            for(var i = 2; i < n; ++i)
                if (n%i == 0)
                    return false;
            return false;
        }
    }

   
}
