﻿namespace IntermediateDotNet.Simple
{
    // In some cases you might have a constructor with compulsory/read only properties.
    public class Customer
    {
        private readonly int id;

        public Customer(int id)
        {
            this.id = id;
        }

        internal int Id
        {
            get { return id; }
        }
        public string Name { get; set; }
        public Address Address { get; set; }
    }
}
