﻿using System;

namespace IntermediateDotNet.Simple
{
    // If possible use structs for values. If needed, You could break Microsoft's recommendation for properties to be read only.
    // Read write properties for structs is fine.

    public struct Point
    {
        private readonly int x;
        private readonly int y;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public Point(int x)
            : this(x, 0)
        {
            
        }

        public int X
        {
            get { return x; }
        }

        public int Y
        {
            get { return y; }
        }

        public double Distance(Point pt)
        {
            var dx = pt.x - x;
            var dy = pt.y - y;
            return Math.Sqrt(dx*dx + dy*dy);
        }

        // return new object instead of providing mutating methods.
        public Point MoveBy(int dx, int dy)
        {
            return new Point(x + dx, y + dy);
        }
    }
}
