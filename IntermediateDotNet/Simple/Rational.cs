﻿using System;

namespace IntermediateDotNet.Simple
{
    //values can be represented by classes, with all fields as readonly. 
    public class Rational
    {
        private readonly int numerator;
        private readonly int denominator;

        public Rational(int numerator, int denominator)
        {
            if(denominator == 0)
                throw new ArgumentException("denominator cannot be zero");
            this.numerator = numerator;
            this.denominator = denominator;
        }

        public int Numerator
        {
            get { return numerator; }
        }

        public int Denominator
        {
            get { return denominator; }
        }

        public static Rational operator +(Rational first, Rational second)
        {
            return new Rational(
                first.numerator * second.denominator + first.denominator * second.numerator,
                first.denominator * second.denominator);
        }

        public static Rational operator -(Rational first, Rational second)
        {
            return new Rational(
                first.numerator * second.denominator - first.denominator * second.numerator,
                first.denominator * second.denominator);
        }

        public static Rational operator *(Rational first, Rational second)
        {
            return new Rational(first.numerator * second.numerator, 
                first.denominator * second.denominator);
        }

        public static Rational operator /(Rational first, Rational second)
        {
            return new Rational(first.numerator * second.denominator,
                first.denominator * second.numerator);
        }

        public static explicit operator double(Rational r)
        {
            return ((double) r.numerator)/r.denominator;
        }
    }
}
