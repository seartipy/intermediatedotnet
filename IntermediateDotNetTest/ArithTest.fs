﻿module Test.Arith

open FsUnit
open NUnit.Framework

open IntermediateDotNet.Simple

[<Test>]
let FactorialTest() =
    let x = 1
    Arith.Factorial 0 |> should equal 1

[<Test>]
let ``Factorial of 1 is 1``() =
    Arith.Factorial 1 |> should equal 1

[<Test>]
let ``Factorial of 5 is 120``() =
    Arith.Factorial 5 |> should equal 120

[<Test>]
let ``1 is not a prime no``() =
    Arith.IsPrime 1 |> should equal false    

