﻿module Query
    
    type IQuery =
        abstract apply : string -> bool;
        abstract toStr : unit -> string;

    type WordQuery(word) =
        interface IQuery with
            member wq.apply line = line.Contains(word)
            member wq.toStr() = word

    type AndQuery(first : IQuery, second : IQuery) = 
        interface IQuery with
            member wq.apply line = first.apply(line) && second.apply(line)
            member wq.toStr() = "(" + first.toStr() + " && " + second.toStr() + ")"

    type OrQuery(first : IQuery, second : IQuery) = 
        interface IQuery with
            member wq.apply line = first.apply(line) || second.apply(line)
            member wq.toStr() = "(" + first.toStr() + " || " + second.toStr() + ")"

 
    type NotQuery(query : IQuery) =
        interface IQuery with
            member wq.apply line = not(query.apply(line))
            member wq.toStr() = "!" + query.toStr()

    let q w = WordQuery(w)
    let (&&&) first second = AndQuery(first, second)
    let (|||) first second = OrQuery(first, second)
    let (~~) query = NotQuery(query)
    let eval query line = (query :> IQuery).apply(line)
    let (<<<) query line = eval query line

    let query = (q "hello") &&& (q "world") ||| (q "universe" &&& ~~(q "planet")) :> IQuery

    printfn "%A" (query <<< "hello world")

    "hello world" 
    |> eval query 
    |> printfn "%A"

    let publicInstanceMembers = q "public" &&& ~~(q "static") |> eval
    let privateStaticMembers =  (q "static") &&& ~~(q "public") |> eval
    let instanceFields = q ";" &&& ( q "private" ||| q "public" ) |> eval