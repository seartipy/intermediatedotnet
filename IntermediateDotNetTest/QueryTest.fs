﻿module QueryTest

open FsUnit
open NUnit.Framework 
open IntermediateDotNet.Query
open System.Linq

let lines = [ 
    "hello world"
    "hello universe"
    "bye world"
    "bye universe"
    "hello planet"
]

[<Test>]
let ``WordQuery query matches all lines containing the word``() =
    Query("hello").Eval(lines).Count()
    |> should equal 3

[<Test>]
let ``AndQuery includes lines included in both queries``() =        
    (Query("hello") &&& Query("world")).Eval(lines).Count()
    |> should equal 1

[<Test>]
let ``OrQuery includes lines from both queries``() =
    (Query("hello") ||| Query("bye")).Eval(lines).Count()
    |> should equal 5

[<Test>]
let ``NotQuery excludes all lines from the query``() =
    (~~~Query("hello")).Eval(lines).Count()
    |> should equal 2
