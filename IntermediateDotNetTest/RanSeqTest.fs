﻿module Test.RanSeq

open FsUnit
open NUnit.Framework

open SeqVals

open IntermediateDotNet.ColFx

[<Test>]
let ``BinarySearch returns last on empty range``() = 
    ranEmpty.BinarySearch(100) |> should equal ranEmpty.Last

[<Test>]
let ``BinarySearch finds the only element in range``() = 
    ranSingle.BinarySearch(1) |> should equal ranSingle.First
    ranSingle.BinarySearch(2) |> should equal ranSingle.Last

[<Test>]
let ``BinarySearch returns seq to value if value found in range``() = 
    ranRange.BinarySearch(5).Current |> should equal 5

[<Test>]
let ``BinarySearch returns last if value not in range``() = 
    ranRange.BinarySearch(100) |> should equal ranRange.Last



