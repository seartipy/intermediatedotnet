﻿module Test.Seq

open FsUnit
open NUnit.Framework

open SeqVals

open IntermediateDotNet.ColFx

[<Test>]
let ``Contains returns false on empty range``() = 
    empty.Contains(100) |> should equal false

[<Test>]
let ``Contains finds the only element in range``() = 
    single.Contains(1) |> should equal true
    single.Contains(2) |> should equal false

[<Test>]
let ``Contains returns true if value found in range``() = 
    range.Contains(5) |> should equal true

[<Test>]
let ``Contains returns false if value not in range``() = 
    empty.Contains(100) |> should equal false
