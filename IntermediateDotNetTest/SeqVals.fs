﻿module SeqVals

open IntermediateDotNet.ColFx

let range = Range<ISeq<int>>(IntSeq(1), IntSeq(10))
let biRange = Range<IBiSeq<int>>(IntSeq(1), IntSeq(10))
let ranRange = Range<IRanSeq<int>>(IntSeq(1), IntSeq(10)) :> IRange<IRanSeq<int>>

let empty = Range<ISeq<int>>(IntSeq(1), IntSeq(1))
let biEmpty = Range<IBiSeq<int>>(IntSeq(1), IntSeq(1))
let ranEmpty = Range<IRanSeq<int>>(IntSeq(1), IntSeq(1)) :> IRange<IRanSeq<int>>

let single = Range<ISeq<int>>(IntSeq(1), IntSeq(2))
let biSingle = Range<IBiSeq<int>>(IntSeq(1), IntSeq(2))
let ranSingle = Range<IRanSeq<int>>(IntSeq(1), IntSeq(2)) :> IRange<IRanSeq<int>>
