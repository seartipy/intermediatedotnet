﻿module VectorTest

open System
open FsUnit
open NUnit.Framework
open IntermediateDotNet.ColFx

let empty() = Vector()
let singleton() = Vector(1)
let vector() = Vector([|1 .. 10|])

[<Test>]
let ``Push on Empty Vector adds the only element``() =
    let v = empty()
    v.Push 100
    v.[0] |> should equal 100

[<Test>]
let ``Push adds element at tails``() =
    let v = Vector()
    v.Push 100
    v.[v.Count - 1] |> should equal 100

[<Test>]
let ``Pop on empty vector throws exception``() =
    let v = empty()
    (fun () -> v.Pop() |> ignore) |> should throw typeof<IndexOutOfRangeException>

[<Test>]
let ``Insert at 0 on empty is same as Push``() =
    let v = Vector()
    v.Insert(0, 100)
    v.[0] |> should equal 100

[<Test>]
let ``Insert(index) adds element at index``() =
    let v = vector()
    v.Insert(5, 100)
    v.Count |> should equal 11
    v.[5] |> should equal 100
    
[<Test>]
let ``RemoveAt(index) removes element at index``() =
    let v = vector()
    v.RemoveAt(5)    
    v.Count |> should equal 9
    v.[5] |> should equal 7

[<Test>]
let ``RemoveAt on empty vector throws exception``() =
    let v = empty()
    (fun () -> v.RemoveAt(0) |> ignore) |> should throw typeof<IndexOutOfRangeException>
